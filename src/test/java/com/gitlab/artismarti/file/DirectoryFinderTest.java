package com.gitlab.artismarti.file;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * @author Artur
 */
public class DirectoryFinderTest {

	@Test
	public void findTheSrcDir() throws IOException {
		DirectoryFinder dirFinder = new DirectoryFinder("src");

		Files.walkFileTree(Paths.get("./"), dirFinder);
		Set<Path> foundMatchingFiles = dirFinder.getFoundMatchingFiles();

		assertThat(foundMatchingFiles.size(), is(1));
		foundMatchingFiles.spliterator()
						.tryAdvance(path -> assertThat(path.getFileName().toString(), is("src")));
	}
}
