package com.gitlab.artismarti.file;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;

/**
 * @author Artur
 */
public class FileUtilsTest {

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	@Test
	public void testListFiles() throws Exception {
		List<Path> paths = FileUtils.listFiles(Paths.get("./"));
		assertThat(paths.size(), greaterThan(15));
	}

	@Test
	public void testListFilesWithSuffix() throws Exception {
		List<Path> paths = FileUtils.listFilesWithSuffix(Paths.get("./"), ".java");
		assertThat(paths.size(), greaterThan(15));
	}

	@Test
	public void testListFilesWithGlobPattern() throws Exception {
		List<Path> paths = FileUtils.listFilesWithGlobRegex(Paths.get("./"), "*Finder*.java");
		assertThat(paths.size(), greaterThanOrEqualTo(4));
	}

	@Test
	public void testListFilesRecursively() throws Exception {
		List<Path> paths = FileUtils.listFilesRecursively(Paths.get("./"));
		assertThat(paths.size(), greaterThan(15));
	}

	@Test
	public void testListFilesRecursivelyWithSuffix() throws Exception {
		List<Path> paths = FileUtils.listFilesRecursivelyWithSuffix(Paths.get("./"), ".java");
		assertThat(paths.size(), greaterThan(15));
	}

	@Test
	public void testListFilesRecursivelyWithRegex() throws Exception {
		List<Path> paths = FileUtils.listFilesRecursivelyWithGlobRegex(Paths.get("./"), "*Finder*.java");
		assertThat(paths.size(), greaterThanOrEqualTo(4));
	}

	@Test
	public void testDeleteFiles() throws Exception {
		File root = folder.newFolder();
		Path rootPath = Paths.get(root.toString());
		Files.createFile(rootPath.resolve("temp1"));
		Files.createFile(rootPath.resolve("temp2"));
		Files.createDirectory(rootPath.resolve("folder"));
		FileUtils.deleteFiles(rootPath);

		assertThat(Files.exists(rootPath), is(false));
	}

	@Test
	public void testDeleteFilesIfPathExist() throws Exception {
		Path rootPath = Paths.get(folder.toString(), "doesNotExist");
		FileUtils.deleteFilesIfPathExist(rootPath);
	}

	@Test(expected = IOException.class)
	public void deleteFilesOnNonExistingFolderExpectIOException() throws IOException {
		Path rootPath = Paths.get(folder.toString(), "doesNotExist");
		FileUtils.deleteFiles(rootPath);
	}
}
