package com.gitlab.artismarti.file;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Artur
 */
public class FileDeleterTest {

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	private File targetDir;

	@Before
	public void setUp() throws IOException {
		targetDir = folder.newFile("classes");
		new File(targetDir, "Test.xml");
		new File(targetDir, "Test2.xml");
	}

	@Test
	public void testDeleteAllFilesInTargetFoldersMatchingPattern() throws Exception {
		FileDeleter.deleteAllFilesInTargetFoldersMatchingPattern(Collections.singletonList(targetDir), "*.xml");

		assertThat(targetDir.listFiles(), is(nullValue()));
	}
}
