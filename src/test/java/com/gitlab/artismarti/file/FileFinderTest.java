package com.gitlab.artismarti.file;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * @author Artur
 */
public class FileFinderTest {

	private static final String FILE_FINDER_JAVA = "FileFinder.java";
	private static final String POM_XML = "pom.xml";

	@Test
	public void findTheFileQuide() throws IOException {
		FileFinder fileFinder = new FileFinder(FILE_FINDER_JAVA);
		Path pathToQuide = Paths.get("./src/main/java/com/gitlab/artismarti/file/FileFinder.java");

		Files.walkFileTree(Paths.get("./"), fileFinder);
		Set<Path> foundMatchingFiles = fileFinder.getFoundMatchingFiles();
		Path foundPath = fileFinder
						.matchEndingOfFoundFilesWithGivenPath(Paths.get("com/gitlab/artismarti/file/FileFinder.java")).get();

		assertThat(foundMatchingFiles.size(), is(1));
		foundMatchingFiles.spliterator()
						.tryAdvance(path -> assertThat(path.endsWith("FileFinder.java"), is(true)));
		assertThat(foundPath, is(pathToQuide));
	}

	@Test
	public void findThePomFileAndSubPathTheEnding() throws IOException {
		FileFinder fileFinder = new FileFinder(POM_XML);

		Files.walkFileTree(Paths.get("./"), fileFinder);
		Set<Path> foundMatchingFiles = fileFinder.getDirsOfMatchingFilesFound();

		assertThat(foundMatchingFiles.size(), is(1));
		foundMatchingFiles.spliterator()
						.tryAdvance(path -> assertThat(path.endsWith(POM_XML), is(false)));
	}

}
