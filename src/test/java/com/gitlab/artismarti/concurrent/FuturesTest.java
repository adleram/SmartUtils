package com.gitlab.artismarti.concurrent;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.fail;

/**
 * @author artur
 */
public class FuturesTest {

	@Test
	public void testMerge() throws Exception {
		ExecutorService executor = Executors.newFixedThreadPool(4);

		List<String> names = Arrays.asList(
				"Artur", "Steffen", "Chris", "Xenia"
		);

		List<CompletableFuture<String>> relevanceFutures = names.stream().
				map(name -> CompletableFuture.supplyAsync(() -> hello(name), executor)).
				map(contentFuture -> contentFuture.thenApply(this::end)).
				map(docFuture -> docFuture.thenCompose(this::isThisTheEnd)).
				collect(Collectors.toList());

		CompletableFuture<List<String>> merged = Futures.merge(relevanceFutures);
		@SuppressWarnings("unchecked") CompletableFuture<List<String>> mergedFromArray =
				Futures.merge(relevanceFutures.toArray(new CompletableFuture[0]));
		merged.get().forEach(line ->
				assertThat(line, containsString("why no ending :("))
		);
		mergedFromArray.get().forEach(line ->
				assertThat(line, containsString("why no ending :("))
		);
	}

	private CompletableFuture<String> isThisTheEnd(String content) {
		return CompletableFuture.supplyAsync(() -> content + "\n why no ending :(\n");
	}

	private String end(String content) {
		return content + ",\n\tby me";
	}

	private String hello(String name) {
		return "Hello from " + name;
	}

	@Test
	public void testGetNowIsDone() throws Exception {
		CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> hello("Artur"));
		Thread.sleep(5L);
		String now = Futures.getNow(future, () -> "NotMe");
		assertThat(now, containsString("Artur"));
	}

	@Test
	public void testGetNowFromSupplier() throws Exception {
		CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
			try {
				Thread.sleep(2000L);
				return "Artur";
			} catch (InterruptedException e) {
				fail();
				return "Exception";
			}
		});
		String now = Futures.getNow(future, () -> "NotMe");
		assertThat(now, containsString("NotMe"));
	}

}
