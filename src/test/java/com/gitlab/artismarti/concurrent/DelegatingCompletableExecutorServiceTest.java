package com.gitlab.artismarti.concurrent;

import org.junit.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author artur
 */
public class DelegatingCompletableExecutorServiceTest {

	ExecutorService executor = CompletableExecutors.completable(Executors.newFixedThreadPool(4));

	AtomicInteger counter = new AtomicInteger();

	@Test
	public void test() {
		try {
			CompletableFuture<String> future = CompletableFuture.supplyAsync(this::getMessage, executor)
					.thenCombine(CompletableFuture.supplyAsync(this::getMessage, executor), String::concat);

			System.out.println(future.get());
		} catch (InterruptedException | ExecutionException e) {
			System.out.println(e);
		}
		assertThat(true, is(true));
	}

	private String getMessage() {
		return "Message " + counter.incrementAndGet() + " ";
	}

}
