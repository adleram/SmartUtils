package com.gitlab.artismarti.collection;

import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * @author Artur
 */
public class CollectionsTest {

	@Test
	public void testIsNullOrEmpty() throws Exception {
		assertThat(Collections.isEmpty(null), is(true));
		assertThat(Collections.isEmpty(java.util.Collections.emptyList()), is(true));
		assertThat(Collections.isEmpty(java.util.Collections.emptySet()), is(true));
		assertThat(Collections.isEmpty(Arrays.asList(1, 2, 3)), is(false));
	}

	@Test
	public void testNotNullOrEmpty() throws Exception {
		assertThat(Collections.notEmpty(null), is(false));
		assertThat(Collections.notEmpty(java.util.Collections.emptyList()), is(false));
		assertThat(Collections.notEmpty(java.util.Collections.emptySet()), is(false));
		assertThat(Collections.notEmpty(Arrays.asList(1, 2, 3)), is(true));
	}
}
