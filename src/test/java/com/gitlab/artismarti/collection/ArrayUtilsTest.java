package com.gitlab.artismarti.collection;

import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

/**
 * @author artur
 */
public class ArrayUtilsTest {

	@Test
	public void concatTwoArrays() {
		String[] a = {"a", "b", "c"};
		String[] b = {"d", "e"};

		String[] concat = ArrayUtils.concat(a, b);

		assertThat(Arrays.asList(concat), hasSize(5));
	}

}
