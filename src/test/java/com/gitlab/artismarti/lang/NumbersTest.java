package com.gitlab.artismarti.lang;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * @author artur
 */
public class NumbersTest {
	@Test
	public void toInt() throws Exception {
		assertThat(Numbers.toInt("123"), is(123));
		assertThat(Numbers.toInt("---"), is(0));
		assertThat(Numbers.toInt("---", -1), is(-1));
	}

	@Test
	public void toDouble() throws Exception {
		assertThat(Numbers.toDouble("1.0"), is(1.0d));
		assertThat(Numbers.toDouble("---"), is(0.0d));
		assertThat(Numbers.toDouble("---", -1.0), is(-1.0d));
	}

}
