package com.gitlab.artismarti.lang;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * @author Artur
 * @since 0.2
 */
public class StringsTest {

	@Test
	public void testIsNullOrEmpty() throws Exception {
		assertThat(Strings.isEmpty(null), is(true));
		assertThat(Strings.isEmpty(""), is(true));
		assertThat(Strings.isEmpty("abc"), is(false));
	}

	@Test
	public void testNotNullOrEmpty() throws Exception {
		assertThat(Strings.notEmpty(null), is(false));
		assertThat(Strings.notEmpty(""), is(false));
		assertThat(Strings.notEmpty("abc"), is(true));
	}

	@Test
	public void testFrequency() throws Exception {
		assertThat(Strings.frequency("", "Hello"), is(0));
		assertThat(Strings.frequency("HelloWorldHelloWorld", ""), is(0));
		assertThat(Strings.frequency("HelloWorldHelloWorld", "Hello"), is(2));
		assertThat(Strings.frequency("HelloWorldHelloWorld", "l"), is(6));
	}

	@Test
	public void testSubstringBefore() throws Exception {
		assertThat(Strings.substringBefore("Hello;World", ";"), is("Hello"));
		assertThat(Strings.substringBefore(";HelloWorld", ";"), is(""));
		assertThat(Strings.substringBefore("HelloWorld;", ";"), is("HelloWorld"));
	}

	@Test
	public void testSubstringAfter() throws Exception {
		assertThat(Strings.substringAfter("Hello;World", ";"), is("World"));
		assertThat(Strings.substringAfter(";HelloWorld", ";"), is("HelloWorld"));
		assertThat(Strings.substringAfter("HelloWorld;", ";"), is(""));
	}

	@Test
	public void testIsAnagram() throws Exception {
		assertThat(Strings.isAnagram("olleH", "Hello"), is(true));
		assertThat(Strings.isAnagram("OlLeh", "Hello"), is(true));
		assertThat(Strings.isAnagram("iPad", "Paid"), is(true));
		assertThat(Strings.isAnagram("iPad", "Pair"), is(false));
	}
}
