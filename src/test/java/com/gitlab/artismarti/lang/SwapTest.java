package com.gitlab.artismarti.lang;

import com.gitlab.artismarti.utils.Pair;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author artur
 */
public class SwapTest {

	@Test
	public void testSwapWithStrings() throws Exception {
		String a = "A";
		String b = "B";
		b = Swap.values(a, a = b);
		assertThat(a, is("B"));
		assertThat(b, is("A"));
	}

	@Test
	public void testSwapWithInts() throws Exception {
		int a = 1;
		int b = 2;
		b = Swap.values(a, a = b);
		assertThat(a, is(2));
		assertThat(b, is(1));
	}

	@Test
	public void testSwapWithObjects() throws Exception {
		Pair<String, Integer> a = new Pair<>("A", 1);
		Pair<String, Integer> b = new Pair<>("B", 2);
		b = Swap.values(a, a = b);
		assertThat(a.getKey(), is("B"));
		assertThat(b.getKey(), is("A"));
		assertThat(a.getValue(), is(2));
		assertThat(b.getValue(), is(1));
	}
}
