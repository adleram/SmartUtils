package com.gitlab.artismarti.lang;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * @author artur
 */
public class TailCallTest {

	@Test
	public void test() {
		assertThat(add(5, 5), is(10));
	}

	@Test(expected = IllegalStateException.class)
	public void returnHasNoResume() {
		TailCall.ret(5).resume();
	}

	public static int add(int x, int y) {
		return addRec(x, y).eval();
	}

	private static TailCall<Integer> addRec(int x, int y) {
		return y == 0
				? TailCall.ret(x)
				: TailCall.sus(() -> addRec(x + 1, y - 1));
	}
}
