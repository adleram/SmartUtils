package com.gitlab.artismarti.timing;

import com.gitlab.artismarti.fuction.VoidSupplier;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;

/**
 * @author artur
 */
public class MeasureTest {

	public static final VoidSupplier VOID_SUPPLIER = () -> {
		int sum = IntStream.range(0, 100).sum();
		List<Integer> list = new ArrayList<Integer>() {{
			add(5);
			add(6);
			add(3);
			add(7);
			add(1);
		}};
		try {
			Thread.sleep(10L);
		} catch (InterruptedException ignored) {
		}
		list.sort(Integer::compare);
		list.stream().map(i -> i * sum);
	};

	@Test
	public void testMeasureMillis() throws Exception {
		long spend = Measure.timeMillis(VOID_SUPPLIER);
		assertThat(spend, greaterThanOrEqualTo(10L));
	}

	@Test
	public void testMeasureNanos() throws Exception {
		long spend = Measure.timeNanos(VOID_SUPPLIER);
		assertThat(spend, greaterThanOrEqualTo(10_000_000L));
	}

	@Test
	public void testTenTimesReturnAverageInMillis() throws Exception {
		long average = Measure.tenTimesReturnAverageInMillis(VOID_SUPPLIER);
		assertThat(average, greaterThanOrEqualTo(10L));
	}

	@Test
	public void testNTimesReturnAverageInMillis() throws Exception {
		long average = Measure.nTimesReturnAverageInMillis(3, VOID_SUPPLIER);
		assertThat(average, greaterThanOrEqualTo(10L));
	}

	@Test
	public void testTenTimesReturnAverageInNanos() throws Exception {
		long average = Measure.tenTimesReturnAverageInNanos(VOID_SUPPLIER);
		assertThat(average, greaterThanOrEqualTo(10_000_000L));
	}

	@Test
	public void testNTimesReturnAverageInNanos() throws Exception {
		long average = Measure.nTimesReturnAverageInNanos(3, VOID_SUPPLIER);
		assertThat(average, greaterThanOrEqualTo(10_000_000L));
	}
}
