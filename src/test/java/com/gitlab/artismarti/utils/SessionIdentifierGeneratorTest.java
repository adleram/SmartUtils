package com.gitlab.artismarti.utils;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Artur
 */
public class SessionIdentifierGeneratorTest {

	@Test
	public void generateThousandDifferentSessionTokenWhichAreAllNonEqual() {
		String referenceToken = SessionIdentifierGenerator.nextSessionId();
		for (int i = 0; i < 1_000; i++) {
			String token = SessionIdentifierGenerator.nextSessionId();
			assertThat(referenceToken, not(token));
		}
	}

}
