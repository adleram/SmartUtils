package com.gitlab.artismarti.utils;

import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * @author Artur
 */
public class DateUtilsTest {

    private static DateTimeFormatter formatter =
            DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL);

    private static final LocalDate LOCAL_DATE = LocalDate.of(2015, Month.DECEMBER, 15);
    private static final String STRING_DATE = formatter.format(LOCAL_DATE);
    @SuppressWarnings("deprecation")
    private static final Date OLD_DATE = new Date(115, 11, 15);


    @Test
    public void formatLocalDateToString() throws Exception {
        String format = DateUtils.format(LOCAL_DATE);

        assertThat(format, is(STRING_DATE));
    }

    @Test
    public void convertStringToLocalDate() throws Exception {
        LocalDate date = DateUtils.from(STRING_DATE);

        assertThat(date, is(LOCAL_DATE));
    }

    @Test
    public void testAsDate() throws Exception {
        assertThat(DateUtils.asLocalDate(OLD_DATE), is(LOCAL_DATE));
    }

    @Test
    public void testAsLocalDate() throws Exception {
        assertThat(DateUtils.asDate(LOCAL_DATE), is(OLD_DATE));
    }
}
