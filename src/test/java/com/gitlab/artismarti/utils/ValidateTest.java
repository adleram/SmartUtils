package com.gitlab.artismarti.utils;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * @author artur
 */
@SuppressWarnings("ConstantConditions")
public class ValidateTest {

	@Test
	public void notEmpty() throws Exception {
		assertThat(Validate.notEmpty("abc"), is("abc"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void emptyString() throws Exception {
		Validate.notEmpty("");
	}

	@Test(expected = NullPointerException.class)
	public void nullString() throws Exception {
		String s = null;
		Validate.notEmpty(s);
	}

	@Test(expected = NullPointerException.class)
	public void nullObject() throws Exception {
		Object o = null;
		Validate.notNull(o);
	}

	@Test(expected = IllegalArgumentException.class)
	public void emptyArrays() throws Exception {
		String[] a = {};
		Validate.notEmpty(a);
	}

	@Test
	public void notEmptyArray() throws Exception {
		String[] a = {"a", "b"};
		Validate.notEmpty(a);
	}

}
