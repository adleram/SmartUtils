package com.gitlab.artismarti.stream;

import com.gitlab.artismarti.testobjects.Dish;
import com.gitlab.artismarti.testobjects.TestObjects;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;

/**
 * @author artur
 */
public class StreamForkerTest {

	List<Dish> dishes = TestObjects.getDishes();

	@Test
	public void testFork() throws Exception {
		Stream<Dish> dishStream = dishes.stream();

		StreamForker.Results results = new StreamForker<>(dishStream)
				.fork("shortMenu", stream -> stream.map(Dish::getName).collect(joining(",")))
				.fork("totalCalories", stream -> stream.mapToInt(Dish::getCalories).sum())
				.fork("mostCaloricDish", stream -> stream.max(comparing(Dish::getCalories)).get())
				.fork("dishesByType", stream -> stream.collect(groupingBy(Dish::getType)))
				.fork("isVegetarian", stream -> stream.filter(Dish::isVegetarian).collect(toList()))
				.getResults();

		String shortMenu = results.get("shortMenu");
		int totalCalories = results.get("totalCalories");
		Dish mostCaloricDish = results.get("mostCaloricDish");
		Map<Dish.Type, List<Dish>> dishesByType = results.get("dishesByType");
		List<Dish> isVegetarian = results.get("isVegetarian");

		assertThat(shortMenu, is("pork,beef,chicken,french fries,rice,season fruit,pizza,prawns,salmon"));
		assertThat(totalCalories, is(4200));
		assertThat(mostCaloricDish.getName(), is("pork"));
		assertThat(dishesByType.entrySet(), hasSize(3));
		assertThat(isVegetarian, hasSize(4));
	}

}
