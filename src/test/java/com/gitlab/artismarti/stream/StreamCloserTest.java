package com.gitlab.artismarti.stream;

import org.junit.Test;

import java.util.stream.Stream;

import static org.junit.Assert.assertTrue;

/**
 * @author artur
 */
public class StreamCloserTest {

	@Test
	public void quietly() throws Exception {
		StreamCloser.quietly(Stream.of("HelloWorld").onClose(this::throwException));
		assertTrue(true);
	}

	private void throwException() {
		throw new RuntimeException();
	}

}
