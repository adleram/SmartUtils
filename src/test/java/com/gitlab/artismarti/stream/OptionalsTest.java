package com.gitlab.artismarti.stream;

import org.junit.Test;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertTrue;

/**
 * @author artur
 */
public class OptionalsTest {

	@Test
	public void test() {
		List<Optional<Integer>> ints = IntStream.range(0, 100)
				.mapToObj(value -> value)
				.map(Optional::of)
				.collect(Collectors.toList());

		List<Integer> result = ints.stream()
				.flatMap(Optionals::stream)
				.collect(Collectors.toList());

		assertTrue(ints.size() == result.size());
	}
}
