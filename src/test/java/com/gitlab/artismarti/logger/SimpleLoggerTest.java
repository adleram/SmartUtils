package com.gitlab.artismarti.logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @author Artur
 */
public class SimpleLoggerTest {

	private static DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
	private static String TEMPLATE_SIMPLE_LOGGER = "./logs/logging_" + dateFormat.format(new Date()) + ".txt";

	private static Logger LOGGER_STRING;
	private static Logger LOGGER_CLASS;
	private static Logger LOGGER_CONSOLE;

	@BeforeClass
	public static void setUp() throws IOException {
		LOGGER_STRING = LogManager.getLogger("SimpleLoggerTest");
		LOGGER_CLASS = LogManager.getLogger(SimpleLoggerTest.class);
		LOGGER_CONSOLE = LogManager.getLogger(ConsoleLogger.class, true);
	}

	@AfterClass
	public static void tearDown() throws Exception {
		Files.deleteIfExists(Paths.get(TEMPLATE_SIMPLE_LOGGER));
		Files.deleteIfExists(Paths.get("./logs"));
	}

	@Test
	public void testWriteLog() throws IOException {
		assertTrue(LOGGER_STRING instanceof SimpleLogger);
		assertTrue(LOGGER_CLASS instanceof SimpleLogger);
		assertTrue(LOGGER_CONSOLE instanceof ConsoleLogger);

		LOGGER_CLASS.info("Test Test Test");
		LOGGER_CLASS.debug("Debug Debug Debug");
		LOGGER_STRING.warn("Warn Warn Warn");
		LOGGER_STRING.error("Error Error Error");

		LOGGER_CONSOLE.info("Test Test Test");
		LOGGER_CONSOLE.debug("Debug Debug Debug");
		LOGGER_CONSOLE.warn("Warn Warn Warn");
		LOGGER_CONSOLE.error("Error Error Error");

		Path file = Paths.get(TEMPLATE_SIMPLE_LOGGER);
		assertTrue(Files.exists(file));
		assertThat(Files.readAllLines(file).size(), is(4));
	}
}
