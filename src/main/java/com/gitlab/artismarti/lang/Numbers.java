package com.gitlab.artismarti.lang;

/**
 * Provides various utility methods to operate on numbers.
 *
 * @author Artur
 * @since 0.3
 */
public final class Numbers {

	/**
	 * Casts a given string to int.
	 *
	 * @param str given string
	 * @return int value of string or 0 as default
	 */
	public static int toInt(final String str) {
		return toInt(str, 0);
	}

	/**
	 * Casts a given string to int else default value.
	 *
	 * @param str          given string
	 * @param defaultValue default int value
	 * @return int value of string or 0 as default
	 */
	public static int toInt(final String str, final int defaultValue) {
		if (str == null) {
			return defaultValue;
		}
		try {
			return Integer.parseInt(str);
		} catch (final NumberFormatException nfe) {
			return defaultValue;
		}
	}

	/**
	 * Casts a given string to double.
	 *
	 * @param str given string
	 * @return double value of string or 0 as default
	 */
	public static double toDouble(final String str) {
		return toDouble(str, 0.0d);
	}

	/**
	 * Casts a given string to double else default value.
	 *
	 * @param str          given string
	 * @param defaultValue default double value
	 * @return double value of string or 0 as default
	 */
	public static double toDouble(final String str, final double defaultValue) {
		if (str == null) {
			return defaultValue;
		}
		try {
			return Double.parseDouble(str);
		} catch (final NumberFormatException nfe) {
			return defaultValue;
		}
	}

}
