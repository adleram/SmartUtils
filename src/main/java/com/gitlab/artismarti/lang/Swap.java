package com.gitlab.artismarti.lang;

/**
 * @author artur
 */
public final class Swap {

	private Swap() {
		throw new InstantiationError();
	}

	/**
	 * Used to values two values. ATTENTION!!! the right use is:
	 * {@code
	 * <p>
	 * a = 1
	 * b = 2
	 * b = values(a, a=b)
	 * <p>
	 * }
	 * <p>
	 * If you just pass values(a, b) nothing will happen!
	 *
	 * @param a   first value
	 * @param b   second value
	 * @param <T> type of values
	 * @return first value which will be the second if you passed x=y as second argument
	 */
	@SuppressWarnings("UnusedParameters")
	public static <T> T values(T a, T b) {
		return a;
	}
}
