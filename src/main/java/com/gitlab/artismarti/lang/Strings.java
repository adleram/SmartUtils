package com.gitlab.artismarti.lang;

import java.util.Arrays;

/**
 * Provides various utility methods to operate on strings.
 *
 * @author Artur
 * @since 0.1
 */
public final class Strings {

	private Strings() throws InstantiationException {
		throw new InstantiationException();
	}

	/**
	 * Checks if a String is {@code null} or empty ({@code ""}).
	 *
	 * @param str The String to check, may be {@code null}.
	 * @return {@code true} if the String is empty or {@code null}, {@code false}
	 * otherwise.
	 */
	public static boolean isEmpty(final String str) {
		return str == null || str.length() == 0;
	}

	/**
	 * Checks if a String is not {@code null} or empty ({@code ""}).
	 *
	 * @param str The String to check, may be {@code null}.
	 * @return {@code true} if the String is not empty or {@code null}, {@code false}
	 * otherwise.
	 */
	public static boolean notEmpty(final String str) {
		return !isEmpty(str);
	}

	/**
	 * Returns the amount of times a substring occurs in a string.
	 *
	 * @param source String to search for a substring
	 * @param part   Substring to count for.
	 * @return Amount of times {@code part} is in {@code source}
	 */
	public static int frequency(final String source, final String part) {

		if (isEmpty(source) || isEmpty(part)) {
			return 0;
		}

		int count = 0;
		for (int pos = 0; (pos = source.indexOf(part, pos)) != -1; count++) {
			pos += part.length();
		}

		return count;
	}

	/**
	 * Returns the substring before the first occurrence of a delimiter. The
	 * delimiter is not part of the result.
	 *
	 * @param string    String to get a substring from.
	 * @param delimiter String to search for.
	 * @return Substring before the first occurrence of the delimiter.
	 */
	public static String substringBefore(final String string, final String delimiter) {

		final int pos = string.indexOf(delimiter);

		return pos >= 0 ? string.substring(0, pos) : string;
	}

	/**
	 * Returns the substring after the first occurrence of a delimiter. The
	 * delimiter is not part of the result.
	 *
	 * @param string    String to get a substring from.
	 * @param delimiter String to search for.
	 * @return Substring after the last occurrence of the delimiter.
	 */
	public static String substringAfter(final String string, final String delimiter) {

		final int pos = string.indexOf(delimiter);

		return pos >= 0 ? string.substring(pos + delimiter.length()) : "";
	}

	/**
	 * Tests if a given string is an anagram of an other string.
	 * Eg. iPad and Pair are anagrams.
	 *
	 * @param thisString  the first string to test
	 * @param otherString the other string to test
	 * @return true if both strings are an anagram
	 */
	public static boolean isAnagram(final String thisString, final String otherString) {
		final char[] s1chars = thisString.toLowerCase().toCharArray();
		final char[] s2chars = otherString.toLowerCase().toCharArray();
		Arrays.sort(s1chars);
		Arrays.sort(s2chars);
		return new String(s1chars).equals(new String(s2chars));
	}

}
