package com.gitlab.artismarti.lang;

import java.util.function.Supplier;

/**
 * Simulates tail-call-optimization with java's supplier capability.
 * A tailcall can either be a Return or a Suspend. Return should be used
 * in exit conditions and Suspend simulates recursion.
 *
 * @author artur
 */
public abstract class TailCall<T> {

	/**
	 * Simulates a stack
	 *
	 * @return the nested tail call
	 */
	public abstract TailCall<T> resume();

	/**
	 * Evaluates the tail call while iterating over all nested tail calls.
	 *
	 * @return the computed value
	 */
	public abstract T eval();

	/**
	 * @return true if its a suspend tail call
	 */
	public abstract boolean isSuspend();

	private TailCall() {
	}

	public static class Return<T> extends TailCall<T> {

		private final T t;

		private Return(T t) {
			this.t = t;
		}

		@Override
		public T eval() {
			return t;
		}

		@Override
		public boolean isSuspend() {
			return false;
		}

		@Override
		public TailCall<T> resume() {
			throw new IllegalStateException("Return has no resume");
		}
	}

	public static class Suspend<T> extends TailCall<T> {

		private final Supplier<TailCall<T>> resume;

		private Suspend(Supplier<TailCall<T>> resume) {
			this.resume = resume;
		}

		@Override
		public T eval() {
			TailCall<T> tailRec = this;
			while (tailRec.isSuspend()) {
				tailRec = tailRec.resume();
			}
			return tailRec.eval();
		}

		@Override
		public boolean isSuspend() {
			return true;
		}

		@Override
		public TailCall<T> resume() {
			return resume.get();
		}
	}

	/**
	 * Represents the return value. Use in break conditions.
	 *
	 * @param t   value
	 * @param <T> type of value
	 * @return wrapped value as a tail call
	 */
	public static <T> Return<T> ret(T t) {
		return new Return<>(t);
	}

	/**
	 * Represents the return value. Use in break conditions.
	 *
	 * @param s   wrapped expression which simulates recursion
	 * @param <T> type of wrapped value
	 * @return wrapped expression as a tail call
	 */
	public static <T> Suspend<T> sus(Supplier<TailCall<T>> s) {
		return new Suspend<>(s);
	}
}
