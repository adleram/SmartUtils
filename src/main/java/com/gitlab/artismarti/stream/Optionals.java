package com.gitlab.artismarti.stream;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author artur
 */
public class Optionals {

	/**
	 * Turns an Optional<T> into a Stream<T> of length zero or one depending upon
	 * whether a value is present.
	 */
	@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
	static <T> Stream<T> stream(Optional<T> opt) {
		return opt.map(Stream::of).orElseGet(Stream::empty);
	}
}
