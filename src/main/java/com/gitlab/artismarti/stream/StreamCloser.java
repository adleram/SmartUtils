package com.gitlab.artismarti.stream;

import java.util.stream.Stream;

/**
 * Closes a stream exception free. Better use try-with-resource in java.
 * Mainly usable for groovy when handling a stream of paths.
 *
 * @author Artur
 * @since 0.3
 */
public final class StreamCloser {

	/**
	 * Closes a stream quietly.
	 *
	 * @param stream given stream
	 * @param <T>    type of stream
	 */
	public static <T> void quietly(Stream<T> stream) {
		try {
			stream.close();
		} catch (Exception ignored) {
		}
	}
}
