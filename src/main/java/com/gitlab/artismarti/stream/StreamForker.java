package com.gitlab.artismarti.stream;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Spliterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Allows to consume a stream executing more than one operation.
 * <p>
 * <pre>{@code
 *     Stream<MyData> stream = myCollection.stream();
 *     StreamForker.Results results = new StreamForker(stream)
 *          .fork(keyAsObject, stream -> stream... operation1)
 *          .fork(keyAsObject2, stream -> stream....operation2)
 *          .fork(keyAsObject3, stream -> stream....operation3)
 *          ...
 *          .getResults();
 *
 *     MyData data = results.get(keyAsObject);
 * }</pre>
 * <p>
 * <p>
 * <p>
 * Implementation used from the book Java 8 in Action.
 *
 * @author artur
 */
public class StreamForker<T> {

	private final Stream<T> stream;
	private final Map<Object, Function<Stream<T>, ?>> forks = new HashMap<>();

	/**
	 * Creates a new stream forker, consuming the given stream.
	 *
	 * @param stream stream which should be forked
	 */
	public StreamForker(Stream<T> stream) {
		this.stream = stream;
	}

	/**
	 * Forks an operation on a stream by supplying a function consuming a stream returning ?.
	 *
	 * @param key      key to identify specific result from the {@link Results} object.
	 * @param function a function to operate on a stream
	 * @return this stream forker
	 */
	public StreamForker<T> fork(Object key, Function<Stream<T>, ?> function) {
		forks.put(key, function);
		return this;
	}

	/**
	 * Consuming the underlying stream while executing all saved fork operations asynchronous.
	 *
	 * @return {@link Results}, containing all actions
	 */
	public Results getResults() {
		ForkingStreamConsumer<T> consumer = build();
		try {
			stream.sequential().forEach(consumer);
		} finally {
			consumer.finish();
		}
		return consumer;
	}

	private ForkingStreamConsumer<T> build() {
		List<BlockingQueue<T>> queues = new ArrayList<>();

		Map<Object, Future<?>> actions = forks.entrySet().stream().reduce(
				new HashMap<>(),
				(map, e) -> {
					map.put(e.getKey(), getOperationResult(queues, e.getValue()));
					return map;
				},
				(map1, map2) -> {
					map1.putAll(map2);
					return map1;
				});
		return new ForkingStreamConsumer<>(queues, actions);
	}

	private Future<?> getOperationResult(List<BlockingQueue<T>> queues, Function<Stream<T>, ?> operation) {
		BlockingQueue<T> queue = new LinkedBlockingQueue<>();
		queues.add(queue);
		Spliterator<T> spliterator = new BlockingQueueSpliterator<>(queue);
		Stream<T> source = StreamSupport.stream(spliterator, false);
		return CompletableFuture.supplyAsync(() -> operation.apply(source));
	}

	/**
	 * Represents all results from the consumed stream. Results can be extracted via the get(objectKey)
	 * method. eg. Key is a String: {@code MyData data = results.get("myData")}.
	 */
	public interface Results {

		/**
		 * Returns a result from the executed operation .
		 *
		 * @param key any object can be a key
		 * @param <R> type of the return type
		 * @return specified type
		 */
		<R> R get(Object key);
	}

	@SuppressWarnings("unchecked")
	private static class ForkingStreamConsumer<T> implements Consumer<T>, Results {

		private static final Object END_OF_STREAM = new Object();

		private final List<BlockingQueue<T>> queues;
		private final Map<Object, Future<?>> actions;

		public ForkingStreamConsumer(List<BlockingQueue<T>> queues, Map<Object, Future<?>> actions) {
			this.queues = queues;
			this.actions = actions;
		}

		@Override
		public void accept(T t) {
			queues.forEach(queue -> queue.add(t));
		}

		private void finish() {
			accept((T) END_OF_STREAM);
		}

		@Override
		public <R> R get(Object key) {
			try {
				return ((Future<R>) actions.get(key)).get();
			} catch (InterruptedException | ExecutionException e) {
				throw new RuntimeException(e);
			}
		}
	}

	private class BlockingQueueSpliterator<E> implements Spliterator<E> {

		private final BlockingQueue<E> queue;

		private BlockingQueueSpliterator(BlockingQueue<E> queue) {
			this.queue = queue;
		}

		@Override
		public boolean tryAdvance(Consumer<? super E> action) {
			E element;
			while (true) {
				try {
					element = queue.take();
					break;
				} catch (InterruptedException ignored) {
				}
			}

			if (element != ForkingStreamConsumer.END_OF_STREAM) {
				action.accept(element);
				return true;
			}

			return false;
		}

		@Override
		public Spliterator<E> trySplit() {
			return null;
		}

		@Override
		public long estimateSize() {
			return 0;
		}

		@Override
		public int characteristics() {
			return 0;
		}
	}
}
