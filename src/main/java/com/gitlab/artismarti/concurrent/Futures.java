package com.gitlab.artismarti.concurrent;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Utility methods to work with futures.
 *
 * @author artur
 */
public final class Futures {

	private Futures() {
		throw new InstantiationError();
	}

	/**
	 * Zips a list of futures to a future containing all results.
	 *
	 * @param futures list of futures
	 * @param <T>     type of the futures
	 * @return a future containing a list of all results
	 */
	public static <T> CompletableFuture<List<T>> merge(List<CompletableFuture<T>> futures) {
		CompletableFuture<Void> allDoneFuture =
				CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()]));
		return allDoneFuture.thenApply(future ->
				futures.stream().
						map(CompletableFuture::join).
						collect(Collectors.toList())
		);
	}

	/**
	 * Zips an array of futures to a future containing all results.
	 *
	 * @param futures list of futures
	 * @param <T>     type of the futures
	 * @return a future containing a list of all results
	 */
	public static <T> CompletableFuture<List<T>> merge(CompletableFuture<T>... futures) {
		return CompletableFuture.allOf(futures)
				.thenApply(future -> Arrays.stream(futures)
						.map(CompletableFuture::join)
						.collect(Collectors.toList())
				);
	}

	/**
	 * Returns the result from the future. If absent result is brought from the supplier.
	 *
	 * @param future        future with result
	 * @param valueIfAbsent value if result cannot be extracted from future
	 * @param <T>           type of future
	 * @return result from future or from supplier
	 * @throws ExecutionException   if this future completed exceptionally
	 * @throws InterruptedException if the current thread was interrupted
	 */
	public static <T> T getNow(CompletableFuture<T> future, Supplier<T> valueIfAbsent)
			throws ExecutionException, InterruptedException {
		if (future.isDone()) {
			return future.get();
		} else {
			return valueIfAbsent.get();
		}
	}
}
