package com.gitlab.artismarti.collection;

import java.util.stream.Stream;

/**
 * Provides various utility methods to operate on arrays.
 *
 * @author Artur
 * @since 0.3
 */
public final class ArrayUtils {

	private ArrayUtils() {}

	/**
	 * Concatenates two arrays.
	 * @param a first array
	 * @param b second array
	 * @return array containing both given arrays
	 */
	public static String[] concat(String[] a, String... b) {
		return Stream.of(a, b).flatMap(Stream::of).toArray(String[]::new);
	}
}
