package com.gitlab.artismarti.collection;

import java.util.Collection;

/**
 * Provides various utility methods to operate on collections.
 *
 * @author Artur
 * @since 0.1
 */
public final class Collections {

	private Collections() throws InstantiationException {
		throw new InstantiationException();
	}

	/**
	 * Checks if a String is {@code null} or empty.
	 *
	 * @param collection The Collection to check, may be {@code null}.
	 * @return {@code true} if the Collection is empty or {@code null}, {@code false}
	 * otherwise.
	 */
	public static boolean isEmpty(Collection<?> collection) {
		return collection == null || collection.isEmpty();
	}

	/**
	 * Checks if a String is not {@code null} or empty.
	 *
	 * @param collection The Collection to check, may be {@code null}.
	 * @return {@code true} if the Collection is not empty or {@code null}, {@code false}
	 * otherwise.
	 */
	public static boolean notEmpty(Collection<?> collection) {
		return !isEmpty(collection);
	}
}
