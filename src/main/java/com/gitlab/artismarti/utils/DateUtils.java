package com.gitlab.artismarti.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;

/**
 * @author Artur
 */
public final class DateUtils {

	private static DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL);

	public static Date asDate(LocalDate localDate) {
		return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}

	public static LocalDate asLocalDate(Date date) {
		return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
	}

	public static String format(LocalDate registerDate) {
		return registerDate != null ? formatter.format(registerDate) : "";
	}

	public static LocalDate from(String dateString) {
		return dateString != null ? LocalDate.from(formatter.parse(dateString)) : LocalDate.now();
	}
}
