package com.gitlab.artismarti.utils;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Generates session identifiers which will work as a token for a token based service.
 * A token is created out of a 130 bits strong secure random generator and parsed to a
 * base-32 representation.
 *
 * @author Artur
 */
public final class SessionIdentifierGenerator {

	private final static SecureRandom random = new SecureRandom();

	private SessionIdentifierGenerator() throws InstantiationException {
		throw new InstantiationException();
	}

	/**
	 * Note that this is a expensive operation.
	 *
	 * @return the next token
	 */
	public static String nextSessionId() {
		return new BigInteger(130, random).toString(32);
	}
}
