package com.gitlab.artismarti.utils;

/**
 * Provides various utility methods to validate objects. Inspired by guava.
 *
 * @author Artur
 * @since 0.3
 */
public final class Validate {

	public static final String ARGUMENT_EMPTY = "Argument expected not to be empty!";
	private static final String ARGUMENT_NULL = "Argument expected not to be null!";

	private Validate() {
	}

	/**
	 * Verifies that the given {@code CharSequence} is not {@code null} or empty.
	 *
	 * @param s the given {@code CharSequence}.
	 * @return the validated {@code CharSequence}.
	 * @throws NullPointerException     if the given {@code CharSequence} is {@code null}.
	 * @throws IllegalArgumentException if the given {@code CharSequence} is empty.
	 */
	public static CharSequence notEmpty(CharSequence s) {
		return notEmpty(s, ARGUMENT_EMPTY);
	}

	/**
	 * Verifies that the given {@code CharSequence} is not {@code null} or empty.
	 *
	 * @param s       the given {@code CharSequence}.
	 * @param message error message in case of empty {@code String}.
	 * @return the validated {@code CharSequence}.
	 * @throws NullPointerException     if the given {@code CharSequence} is {@code null}.
	 * @throws IllegalArgumentException if the given {@code CharSequence} is empty.
	 */
	public static CharSequence notEmpty(CharSequence s, String message) {
		notNull(s, message);
		if (s.length() == 0) throw new IllegalArgumentException(message);
		return s;
	}

	/**
	 * Verifies that the given array is not {@code null} or empty.
	 *
	 * @param array the given array.
	 * @return the validated array.
	 * @throws NullPointerException     if the given array is {@code null}.
	 * @throws IllegalArgumentException if the given array is empty.
	 */
	public static <T> T[] notEmpty(T[] array) {
		T[] checked = notNull(array);
		if (checked.length == 0) throw new IllegalArgumentException(ARGUMENT_EMPTY);
		return checked;
	}

	/**
	 * Verifies that the given object reference is not {@code null}.
	 *
	 * @param reference the given object reference.
	 * @return the non-{@code null} reference that was validated.
	 * @throws NullPointerException if the given object reference is {@code null}.
	 */
	public static <T> T notNull(T reference) {
		if (reference == null) throw new NullPointerException(ARGUMENT_NULL);
		return reference;
	}

	/**
	 * Verifies that the given object reference is not {@code null}.
	 *
	 * @param reference the given object reference.
	 * @param message   error message in case of null reference.
	 * @return the non-{@code null} reference that was validated.
	 * @throws NullPointerException if the given object reference is {@code null}.
	 */
	public static <T> T notNull(T reference, String message) {
		if (reference == null) throw new NullPointerException(message);
		return reference;
	}

}
