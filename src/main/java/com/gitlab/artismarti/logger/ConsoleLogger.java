package com.gitlab.artismarti.logger;

/**
 * @author Artur
 */
class ConsoleLogger extends Logger {

	ConsoleLogger(String logName) {
		super(logName);
	}

	public void info(String msg) {
		System.out.println(print(msg, Level.INFO));
	}

	public void debug(String msg) {
		System.out.println(print(msg, Level.DEBUG));
	}

	public void warn(String msg) {
		System.out.println(print(msg, Level.WARN));
	}

	public void error(String msg) {
		System.err.println(print(msg, Level.ERROR));
	}

	private String print(String msg, Level level) {
		return getTime(getDate()) + " - " + logFileName + " - " + level + " - " + msg;
	}
}
