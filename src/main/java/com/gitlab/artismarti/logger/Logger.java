package com.gitlab.artismarti.logger;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Interface for a basic logger. Provides methods to support a simple logging.
 * To implement a new logger extend this class.
 * <p>
 * Created by artur on 14.05.15.
 */
public abstract class Logger {

	/**
	 * Presents the log level.
	 */
	enum Level {
		INFO("INFO"),
		DEBUG("DEBUG"),
		WARN("WARN"),
		ERROR("ERROR");

		String level;

		Level(String level) {
			this.level = level;
		}
	}


	protected String logFileName;

	private DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
	private DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

	protected Logger(String logName) {
		this.logFileName = logName;
	}

	/**
	 * Writes a info message into the log. This is the common way to
	 * note functionality of your program.
	 *
	 * @param msg the message to log
	 */
	public void info(String msg) {
		log(msg, Level.INFO);
	}

	/**
	 * Writes a debug message into the log. Use for debug purpose.
	 *
	 * @param msg the message to log
	 */
	public void debug(String msg) {
		log(msg, Level.DEBUG);
	}

	/**
	 * Writes a warning message into the log. Use this method if you want
	 * to tell the developers that some functionality is not available.
	 *
	 * @param msg the message to log
	 */
	public void warn(String msg) {
		log(msg, Level.WARN);
	}

	/**
	 * Writes a error message into the log. Should be used if behaviour of the
	 * program is invalid.
	 *
	 * @param msg the message to log
	 */
	public void error(String msg) {
		log(msg, Level.ERROR);
	}

	private void log(String msg, Level level) {
		Date date = getDate();
		writeLog(getTime(date) + " - " + logFileName + " - " + level + " - " + msg, date);
	}

	protected Date getDate() {
		return new Date();
	}

	protected String getTime(Date date) {
		return timeFormat.format(date);
	}

	private void writeLog(String log, Date date) {
		Path path = Paths.get("./logs/logging_" + dateFormat.format(date) + ".txt");
		String append = log + "\n";

		if (!createLogsDir()) {
			return;
		}

		try (BufferedWriter writer = Files.newBufferedWriter(path, Charset.defaultCharset(),
						StandardOpenOption.CREATE, StandardOpenOption.APPEND)) {
			writer.write(append, 0, append.length());
		} catch (IOException e) {
			System.err.format("IOException: %s%n", e);
		}
	}

	private boolean createLogsDir() {
		Path path = Paths.get("./logs");
		if (!Files.exists(path)) {
			try {
				Files.createDirectory(path);
			} catch (IOException e) {
				System.err.format("IOException: %s%n", e);
				return false;
			}
		}
		return true;
	}
}
