package com.gitlab.artismarti.logger;

/**
 * Logger factory, allows to create a logger for a specific purpose.
 *
 * Created by artur on 14.05.15.
 */
public class LogManager {

	private LogManager() throws InstantiationException {
		throw new InstantiationException();
	}

	/**
	 * Returns a simple logger. Log files will be stored
	 * in the folder './logs/'.
	 *
	 * @param logName name for the logger and logfile
	 * @return a simple logger
	 */
	public static Logger getLogger(String logName) {
		return getLogger(logName, false);
	}

	/**
	 * Returns a simple logger. A boolean decides if
	 * the log files will be stored on the filesystem
	 * in the folder './logs/' or only be displayed
	 * on the console.
	 *
	 * @param logName name for the logger and logfile
	 * @param useConsole output will be logged in the console
	 * @return a simple logger
	 */
	public static Logger getLogger(String logName, boolean useConsole) {
		return createLogger(logName, useConsole);
	}

	/**
	 * Returns a simple logger. Log files will be stored
	 * in the folder './logs/'. The difference to getLogger(String logName)
	 * is that the logfile names will be created out of the simple name
	 * of given class.
	 *
	 * @param logClass class which should be logged
	 * @return a simple logger
	 */
	public static  <T> Logger getLogger(Class<T> logClass) {
		return getLogger(logClass, false);
	}

	/**
	 * Returns a simple logger. A boolean decides if
	 * the log files will be stored on the filesystem
	 * in the folder './logs/' or only be displayed
	 * on the console. The difference to getLogger(String logName)
	 * is that the logfile names will be created out of the simple name
	 * of given class.
	 *
	 * @param logClass name for the logger and logfile extracted from the class name
	 * @param useConsole output will be logged in the console
	 * @return a simple logger
	 */
	public static  <T> Logger getLogger(Class<T> logClass, boolean useConsole) {
		return createLogger(logClass.getSimpleName(), useConsole);
	}

	private static Logger createLogger(String logName, boolean useConsole) {
		if (useConsole) {
			return new ConsoleLogger(logName);
		}

		return new SimpleLogger(logName);
	}

}
