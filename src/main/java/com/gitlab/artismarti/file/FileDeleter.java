package com.gitlab.artismarti.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;

/**
 * Provides functionality to delete recursively files matching specific patterns.
 *
 * @author Artur
 */
public class FileDeleter {

	/**
	 * Deletes all files in the given file list matching the given pattern.
	 *
	 * @param targetFiles list of files
	 * @param pattern     pattern for files to delete
	 * @throws IOException when an i/o exception occurs while walking the file tree
	 */
	public static void deleteAllFilesInTargetFoldersMatchingPattern(List<File> targetFiles, String pattern) throws IOException {
		for (File file : targetFiles) {
			FileFinder fileFinder = new FileFinder(pattern);
			Files.walkFileTree(file.toPath(), fileFinder);
			Set<Path> foundMatchingFiles = fileFinder.getFoundMatchingFiles();
			for (Path matchingFile : foundMatchingFiles) {
				Files.deleteIfExists(matchingFile);
			}
		}
	}
}
