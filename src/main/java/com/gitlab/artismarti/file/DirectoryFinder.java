package com.gitlab.artismarti.file;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Finds directories by traversing a tree using the visitor pattern. Note: This is not thread safe!
 *
 * @author Artur
 */
public class DirectoryFinder extends FileFinder {

	/**
	 * Creates a directory finder with given search pattern
	 *
	 * @param searchedPath search pattern for paths
	 */
	public DirectoryFinder(String searchedPath) {
		super(searchedPath);
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		return super.visitFile(file, attrs);
	}

	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
		addIfMatches(dir);
		return FileVisitResult.CONTINUE;
	}
}
