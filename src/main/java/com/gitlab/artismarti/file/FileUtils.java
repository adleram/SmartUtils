package com.gitlab.artismarti.file;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * Provides useful utility methods to operate on files.
 *
 * @author Artur
 */
public final class FileUtils {

	private FileUtils() throws InstantiationException {
		throw new InstantiationException();
	}

	/**
	 * Lists all files by storing all containing directories in a queue and process them one by one.
	 *
	 * @param path starting directory
	 * @return list of all containing files
	 * @throws IOException exception while processing the stream of files
	 */
	public static List<Path> listFiles(Path path) throws IOException {
		return listFiles(path, path1 -> true);
	}

	/**
	 * Lists all files which matches given suffix by storing all containing directories in a queue and process them one by one.
	 *
	 * @param path   starting directory
	 * @param suffix suffix searched files have to match
	 * @return list of all containing files
	 * @throws IOException exception while processing the stream of files
	 */
	public static List<Path> listFilesWithSuffix(Path path, String suffix) throws IOException {
		PathMatcher pathMatcher = path1 -> path1.toString().endsWith(suffix);
		return listFiles(path, pathMatcher);
	}

	/**
	 * Lists all files which matches given pattern by storing all containing directories in a queue and process them one by one.
	 * Allows to use Unix patterns, e.g.
	 * {@code
	 * String pattern = "*Finder*.java";
	 * }
	 *
	 * @param path    starting directory
	 * @param pattern regex searched files have to match
	 * @return list of all containing files
	 * @throws IOException exception while processing the stream of files
	 */
	public static List<Path> listFilesWithGlobRegex(Path path, String pattern) throws IOException {
		PathMatcher pathMatcher = FileSystems.getDefault().getPathMatcher("glob:" + pattern);
		return listFiles(path, pathMatcher);
	}

	/**
	 * Lists all files which matches given pattern by storing all containing directories in a queue and process them one by one.
	 * Allows to use Unix patterns, e.g.
	 * {@code
	 * PathMatcher matcher = path -> path.endsWith(".java");
	 * }
	 *
	 * @param path        starting directory
	 * @param pathMatcher custom defined matcher
	 * @return list of all containing files
	 * @throws IOException exception while processing the stream of files
	 */
	public static List<Path> listFiles(Path path, PathMatcher pathMatcher) throws IOException {

		Deque<Path> stack = new ArrayDeque<>();
		final List<Path> files = new LinkedList<>();

		stack.push(path);

		while (!stack.isEmpty()) {
			try (DirectoryStream<Path> stream = Files.newDirectoryStream(stack.pop())) {
				for (Path entry : stream) {
					if (Files.isDirectory(entry)) {
						stack.push(entry);
					} else {
						if (pathMatcher.matches(entry.getFileName())) {
							files.add(entry);
						}
					}
				}
			}
		}

		return files;
	}

	/**
	 * Lists all files by storing all containing directories in a queue and process them one by one.
	 * This is done recursively. Use this method when you need performance and a stack overflow is not likely.
	 *
	 * @param path starting directory
	 * @return list of all containing files
	 * @throws IOException exception while processing the stream of files
	 */
	public static List<Path> listFilesRecursively(Path path) throws IOException {
		return listFilesRecursively(path, path1 -> true);
	}

	/**
	 * Lists all files which matches given suffix by storing all containing directories in a queue and process them one by one.
	 * This is done recursively. Use this method when you need performance and a stack overflow is not likely.
	 *
	 * @param path   starting directory
	 * @param suffix suffix searched files have to match
	 * @return list of all containing files
	 * @throws IOException exception while processing the stream of files
	 */
	public static List<Path> listFilesRecursivelyWithSuffix(Path path, String suffix) throws IOException {
		PathMatcher pathMatcher = path1 -> path1.toString().endsWith(suffix);
		return listFilesRecursively(path, pathMatcher);
	}

	/**
	 * Lists all files which matches given pattern by storing all containing directories in a queue and process them one by one.
	 * This is done recursively. Use this method when you need performance and a stack overflow is not likely.
	 * Allows to use Unix patterns, e.g.
	 * {@code
	 * String pattern = "*Finder*.java";
	 * }
	 *
	 * @param path    starting directory
	 * @param pattern regex searched files have to match
	 * @return list of all containing files
	 * @throws IOException exception while processing the stream of files
	 */
	public static List<Path> listFilesRecursivelyWithGlobRegex(Path path, String pattern) throws IOException {
		PathMatcher pathMatcher = FileSystems.getDefault().getPathMatcher("glob:" + pattern);
		return listFilesRecursively(path, pathMatcher);
	}

	/**
	 * Lists all files which matches given pattern by storing all containing directories in a queue and process them one by one.
	 * This is done recursively. Use this method when you need performance and a stack overflow is not likely.
	 * Allows to use Unix patterns, e.g.
	 * {@code
	 * PathMatcher matcher = path -> path.endsWith(".java");
	 * }
	 *
	 * @param path        starting directory
	 * @param pathMatcher custom defined matcher
	 * @return list of all containing files
	 * @throws IOException exception while processing the stream of files
	 */
	public static List<Path> listFilesRecursively(Path path, PathMatcher pathMatcher) throws IOException {
		final List<Path> files = new LinkedList<>();
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
			for (Path entry : stream) {
				if (Files.isDirectory(entry)) {
					files.addAll(listFilesRecursively(entry, pathMatcher));
				}
				if (pathMatcher.matches(entry.getFileName())) {
					files.add(entry);
				}
			}
		}
		return files;
	}

	/**
	 * Deletes a directory recursively by deleting all containing files.
	 *
	 * @param path path to directory to delete
	 * @throws IOException if an io exception is thrown while deleting a file
	 */
	public static void deleteFiles(Path path) throws IOException {
		Files.walkFileTree(path, new SimpleFileVisitor<Path>() {

			@Override public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				Files.delete(dir);
				return FileVisitResult.CONTINUE;
			}

		});
	}

	/**
	 * Cleans a directory by deleting recursively all files within the path.
	 *
	 * @param path path to directory to delete
	 * @throws IOException if an io exception is thrown while deleting a file
	 */
	public static void cleanDirectory(Path path) throws IOException {
		Files.walkFileTree(path, new SimpleFileVisitor<Path>() {

			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				if (!Files.isSameFile(path, dir)) {
					Files.delete(dir);
				}
				return FileVisitResult.CONTINUE;
			}

		});
	}

	/**
	 * Deletes a directory recursively by deleting all containing files, but only if the given path exists.
	 *
	 * @param path path to directory to delete
	 * @throws IOException if an io exception is thrown while deleting a file
	 */
	public static void deleteFilesIfPathExist(Path path) throws IOException {
		if (Files.exists(path)) {
			deleteFiles(path);
		}
	}
}
