package com.gitlab.artismarti.file;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Finds files by traversing a tree using the visitor pattern. Note: This is not thread safe!
 *
 * @author Artur
 */
public class FileFinder extends SimpleFileVisitor<Path> {

	private final PathMatcher matcher;
	private Set<Path> foundMatchingFiles;

	/**
	 * Creates a file finder with given search pattern
	 *
	 * @param searchedPath search pattern for paths
	 */
	public FileFinder(String searchedPath) {
		this.matcher = FileSystems.getDefault().getPathMatcher("glob:" + searchedPath);
		this.foundMatchingFiles = new HashSet<>();
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		addIfMatches(file);
		return FileVisitResult.CONTINUE;
	}

	/**
	 * Checks if filename of the given path matches the pattern.
	 *
	 * @param path path to test file name
	 */
	protected void addIfMatches(Path path) {
		Path dirName = path.getFileName();
		if (dirName != null && matcher.matches(dirName))
			foundMatchingFiles.add(path);
	}

	/**
	 * @return a set of all files matching given pattern
	 */
	public Set<Path> getFoundMatchingFiles() {
		return foundMatchingFiles;
	}

	/**
	 * @return a set of parent files of found files matching given pattern
	 */
	public Set<Path> getDirsOfMatchingFilesFound() {
		return foundMatchingFiles.stream()
				.map(path -> path.subpath(0, path.getNameCount() - 1))
				.collect(Collectors.toSet());
	}

	/**
	 * Tests if one of the found matching has an ending like the given path.
	 * This method is useful if you are searching for eg. data.java files.
	 * You get a set of maybe four data.java files. If you know maybe the starting package
	 * of the searched data.java, this method can help you finding the right class.
	 *
	 * @param matchingEnding given path
	 * @return maybe the file for which given path is matching
	 */
	public Optional<Path> matchEndingOfFoundFilesWithGivenPath(Path matchingEnding) {
		return foundMatchingFiles.stream()
				.filter(path -> path.endsWith(matchingEnding))
				.findFirst();
	}
}
