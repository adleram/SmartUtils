package com.gitlab.artismarti.cache;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Defines a generic cache.
 *
 * @author Artur
 */
public abstract class Cache<K, V> {

	/**
	 * Internal representation of the cache.
	 */
	protected Map<K, V> cache = new ConcurrentHashMap<>();

	public void reset() {
		cache.clear();
	}

	public V verifyAndReturn(K value) {
		return cache.getOrDefault(value, defaultValue());
	}

	public void putPair(K key, V value) {
		Objects.requireNonNull(key, "Key must not be null!");
		Objects.requireNonNull(value, "Value must not be null!");
		cache.put(key, value);
	}

	public boolean hasKey(K key) {
		return key != null && cache.containsKey(key);
	}

	public boolean hasValue(V value) {
		return value != null && cache.containsValue(value);
	}

	public int size() {
		return cache.size();
	}

	/**
	 * The value which will be returned if the cache does not contain the requested key. <br/>
	 * Override by concrete caches for different default values
	 * 
	 * @return the default value
	 */
	public V defaultValue() {
		return null;
	}

	/**
	 * Internal representation of this cache.
	 *
	 * @return Internal representation of this cache
	 */
	Map<K, V> getInternalCache() {
		return cache;
	}

}
