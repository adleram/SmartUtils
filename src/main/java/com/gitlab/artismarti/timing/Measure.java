package com.gitlab.artismarti.timing;

import com.gitlab.artismarti.fuction.VoidSupplier;

import java.util.function.LongSupplier;

/**
 * Provides utility methods to measure time for executing code.
 *
 * @author artur
 */
public final class Measure {

	private Measure() {
		throw new InstantiationError();
	}

	/**
	 * Measures the time the given code block needs to execute.
	 *
	 * @param block code block, takes nothing returns void
	 * @return time in millis as long
	 */
	public static long timeMillis(VoidSupplier block) {
		long start = System.currentTimeMillis();
		block.invoke();
		return (System.currentTimeMillis() - start);
	}

	/**
	 * Measures the time the given code block needs to execute.
	 *
	 * @param block code block, takes nothing returns void
	 * @return time in nanos as long
	 */
	public static long timeNanos(VoidSupplier block) {
		long start = System.nanoTime();
		block.invoke();
		return (System.nanoTime() - start);
	}

	/**
	 * Measures ten times the time the given code block needs to execute.
	 *
	 * @param block code block, takes nothing returns void
	 * @return average time in millis as long
	 */
	public static long tenTimesReturnAverageInMillis(VoidSupplier block) {
		return times(10, () -> timeMillis(block));
	}

	/**
	 * Measures n times the time the given code block needs to execute.
	 *
	 * @param block code block, takes nothing returns void
	 * @return average time in millis as long
	 */
	public static long nTimesReturnAverageInMillis(int times, VoidSupplier block) {
		return times(times, () -> timeMillis(block));
	}

	/**
	 * Measures ten times the time the given code block needs to execute.
	 *
	 * @param block code block, takes nothing returns void
	 * @return average time in nanos as long
	 */
	public static long tenTimesReturnAverageInNanos(VoidSupplier block) {
		return times(10, () -> timeNanos(block));
	}

	/**
	 * Measures n times the time the given code block needs to execute.
	 *
	 * @param block code block, takes nothing returns void
	 * @return average time in nanos as long
	 */
	public static long nTimesReturnAverageInNanos(int times, VoidSupplier block) {
		return times(times, () -> timeNanos(block));
	}

	private static long times(int times, LongSupplier measureBlock) {
		long time = 0;
		for (int i = 0; i < times; i++) {
			time += measureBlock.getAsLong();
		}
		return time / times;
	}

}
