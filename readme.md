Collection of useful utility classes and methods.

## Changelog:

### 0.3.x
- Add new utils: Validate, ArrayUtils, Pair, Numbers, StreamCloser

### 0.2.x
- Added StreamForker to apply multiple operation on streams.
- Added my LOC counter project from gitlab (languages: java, cpp)
- Added Futures utility class, providing merge method to zip a list of futures
- Added support to measure time in millis/nanos around code blocks with Measure class

### Up to 0.2.1
- First documented version
- Basic utility operations on Strings/Collections
- List or delete Files with FileUtils
- Find or delete specific files matching a pattern with FileFinder/FileDeleter
- Added simple logger
- Added abstract cache interface and an abstract string to string cache
- Added Date to LocalDate conversion methods -> DateUtils
- Added session id generator
